"""
This module represents a device.

Computer Systems Architecture Course
Assignment 1
March 2016
"""

from threading import Event, Thread, Lock, Semaphore

class ReusableBarrier(object):
    """
    Class that represents a reusable barrier.
    """
    def __init__(self, num_threads):
        """
        Constructor.
        @type num_threads: Integer
        @param num_threads: the number of threads
        """
        self.num_threads = num_threads
        self.count_threads1 = [self.num_threads]
        self.count_threads2 = [self.num_threads]
        self.count_lock = Lock()                 # protejam accesarea/modificarea contoarelor
        self.threads_sem1 = Semaphore(0)         # blocam thread-urile in prima etapa
        self.threads_sem2 = Semaphore(0)         # blocam thread-urile in a doua etapa

    def wait(self):
        """
        Wait for both semaphores
        """
        self.phase(self.count_threads1, self.threads_sem1)
        self.phase(self.count_threads2, self.threads_sem2)

    def phase(self, count_threads, threads_sem):
        """
        num_threads call acquire and the last
        calls release for all the other threads
        """
        with self.count_lock:
            count_threads[0] -= 1
            if count_threads[0] == 0:            # a ajuns la bariera si ultimul thread
                for _ in range(self.num_threads):
                    # incrementarea semaforului va debloca num_threads thread-uri
                    threads_sem.release()
                count_threads[0] = self.num_threads  # reseteaza contorul
        threads_sem.acquire()

class Device(object):
    """
    Class that represents a device.
    """

    def __init__(self, device_id, sensor_data, supervisor):
        """
        Constructor.

        @type device_id: Integer
        @param device_id: the unique id of this node; between 0 and N-1

        @type sensor_data: List of (Integer, Float)
        @param sensor_data: a list containing (location, data)
            as measured by this device

        @type supervisor: Supervisor
        @param supervisor: the testing infrastructure's control
                                    and validation component
        """
        self.device_id = device_id
        self.sensor_data = sensor_data
        self.supervisor = supervisor
        self.script_received = Event()
        self.scripts = []
        self.timepoint_done = Event()
        self.thread = DeviceThread(self)
        self.thread.start()

        self.barrier = None
        self.sem_locations = None

    def __str__(self):
        """
        Pretty prints this device.

        @rtype: String
        @return: a string containing the id of this device
        """
        return "Device %d" % self.device_id


    def setup_devices(self, devices):
        """
        Setup the devices before simulation begins.

        @type devices: List of Device
        @param devices: list containing all devices
        """
        # set barrier for all devices
        if self.device_id == 0:
            barrier = ReusableBarrier(len(devices))
            for device in devices:
                device.barrier = barrier

        if self.sem_locations is None:
            sem_locations = {}
            for device in devices:
                device.sem_locations = sem_locations

        # add locations and semaphores to dictionary
        for location in self.sensor_data.keys():
            self.sem_locations.update({location: Semaphore()})

    def assign_script(self, script, location):
        """
        Provide a script for the device to execute.

        @type script: Script
        @param script: the script to execute from now on at each timepoint;
            None if the current timepoint has ended

        @type location: Integer
        @param location: the location for which the script is interested in
        """
        if script is not None:
            self.scripts.append((script, location))
        else:
            self.script_received.set()
            self.timepoint_done.set()

    def get_data(self, location):
        """
        Returns the pollution value this device has for the given location.

        @type location: Integer
        @param location: a location for which obtain the data

        @rtype: Float
        @return: the pollution value
        """

        return self.sensor_data[location] if location in self.sensor_data else None

    def set_data(self, location, data):
        """
        Sets the pollution value stored by this device for the given location.

        @type location: Integer
        @param location: a location for which to set the data

        @type data: Float
        @param data: the pollution value
        """
        if location in self.sensor_data:
            self.sensor_data[location] = data

    def shutdown(self):
        """
        Instructs the device to shutdown (terminate all threads). This method
        is invoked by the tester. This method must block until all the threads
        started by this device terminate.
        """
        self.thread.join()

class DeviceWorker(Thread):
    """
    Class that represents a thread created by a device.
    """

    def __init__(self, device, worker_id, neighbours):

        Thread.__init__(self, name="Device Thread %d" % device.device_id)
        self.device = device
        self.worker_id = worker_id
        self.neighbours = neighbours
        self.scripts_to_do = []

    def run(self):

        for (script, location) in self.scripts_to_do:

            # syncronize location
            if self.device.sem_locations.get(location) is not None:
                self.device.sem_locations.get(location).acquire()

            script_data = []

            # collect data from current neighbours
            for device in self.neighbours:
                data = device.get_data(location)
                if data is not None:
                    script_data.append(data)

            # add our data, if any
            data = self.device.get_data(location)

            if data is not None:
                script_data.append(data)

            if script_data != []:
                # run script on data
                result = script.run(script_data)

                for device in self.neighbours:
                    device.set_data(location, result)

                self.device.set_data(location, result)

            if self.device.sem_locations.get(location) is not None:
                self.device.sem_locations.get(location).release()


class DeviceThread(Thread):
    """
    Class that implements the device's worker thread.
    """

    def __init__(self, device):
        """
        Constructor.

        @type device: Device
        @param device: the device which owns this thread
        """
        Thread.__init__(self, name="Device Thread %d" % device.device_id)
        self.device = device

    def run(self):

        while True:
            # get the current neighbourhood
            neighbours = self.device.supervisor.get_neighbours()
            if neighbours is None:
                break

            self.device.timepoint_done.wait()

            # compute number of threads
            threads_no = len(self.device.scripts) if len(self.device.scripts) < 8 else 8

            # create thread pool
            thread_pool = []

            for thread_id in range(threads_no):
                thread_pool.append(DeviceWorker(self.device, thread_id, neighbours))

            # share scripts
            if threads_no > 0:
                sc_per_t = len(self.device.scripts) / threads_no
                remains = len(self.device.scripts) % threads_no

                for j in range(threads_no):
                    for i in range(sc_per_t):
                        thread_pool[j].scripts_to_do.append(
                            (self.device.scripts[i + sc_per_t * j][0],
                             self.device.scripts[i + sc_per_t * j][1]))

                for i in range(remains):
                    thread_pool[threads_no - 1].scripts_to_do.append(
                        (self.device.scripts[i + threads_no * sc_per_t][0],
                         self.device.scripts[i + threads_no * sc_per_t][1]))

            # start threads
            for i in range(threads_no):
                thread_pool[i].start()

            for i in range(threads_no):
                thread_pool[i].join()

            # apply barrier
            self.device.barrier.wait()
            self.device.timepoint_done.clear()
